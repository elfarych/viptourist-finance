export default function priceFormatter (price) {
  if (price) {
    return new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'USD'
    }).format(price)
  }
}
