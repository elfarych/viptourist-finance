import { Notify } from 'quasar'

export default function notifier ({ message = '', color = '', position = '' }) {
  Notify.create({
    message,
    color: color || 'dark',
    position: position || 'top'
  })
}
