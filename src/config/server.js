export default {
  // serverURI: 'http://37.140.241.144:1337',
  serverURI: 'http://192.168.0.155:1337',
  reportsServerURI: 'http://192.168.0.155:3000',
  // notificationServerURI: 'http://37.140.241.144:1338',
  notificationServerURI: 'http://192.168.0.155:1338',
  headers: {
    Authorization: ''
  },

  setHeaders (jwt) {
    this.headers.Authorization = `Bearer ${jwt}`
  }
}
