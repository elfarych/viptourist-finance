import enUS from './en'
import ruRU from './ru-RU'

export default {
  en: enUS,
  'ru-RU': ruRU
}
