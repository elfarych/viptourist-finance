export default {
  computed: {
    links () {
      return [
        { routeName: 'orders', title: 'Заказы', icon: 'las la-money-check-alt' },
        { routeName: 'payments', title: 'Выплаты', icon: 'las la-wallet' }
      ]
    }
  }
}
