import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import notifier from 'src/utils/notifier'

export async function loginUser ({ commit, dispatch }, payload) {
  try {
    await axios
      .post(`${server.serverURI}/auth/local`, {
        identifier: payload.email,
        password: payload.password
      }, {
        headers: server.headers
      })
      .then(response => {
        if (response.data.user.role.name === 'Finance' || response.data.user.role.name === 'Superadmin') {
          commit('mutationsUser', response.data.user)
          server.setHeaders(response.data.jwt)
          localStorage.setItem('jwt', response.data.jwt)
          return notifier({ message: 'Авторизация прошла успешно.', color: 'positive' })
        } else {
          return notifier({ message: 'У Вас недостаточно прав.' })
        }
      })
      .catch(e => notifier({ message: e.message, color: 'negative' }))
  } catch (err) {
    errorHandler(err)
  }
}

export async function getUser ({ commit }, jwt) {
  try {
    await axios
      .get(`${server.serverURI}/users/me`, {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
      .then(response => {
        server.setHeaders(jwt)
        commit('mutationsUser', response.data)
        return notifier({ message: 'Авторизация прошла успешно.', color: 'positive' })
      })
  } catch (err) {
    errorHandler(err)
  }
}
