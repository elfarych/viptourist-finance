import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

export async function loadProfile ({ commit }, id) {
  try {
    await axios
      .get(`${server.serverURI}/profiles/${id}`, {
        headers: server.headers
      })
      .then(response => {
        commit('mutationProfile', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
