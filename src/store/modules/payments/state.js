export default function () {
  return {
    payments: [],
    canResetFilters: false,
    filters: {
      searchText: '',
      dateFrom: new Date(new Date().setHours(0)),
      dateTo: new Date(),
      status: 'all',
      statusOptions: [
        { label: 'Все заявки', value: 'all' },
        { label: 'Выплаченные', value: 'completed' },
        { label: 'Отмененные', value: 'canceled' }
      ]
    }
  }
}
