export const filteredPayments = state => {
  return state.payments
    .filter(
      item => item.profile.name?.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.profile.id?.toLowerCase().includes(state.filters.searchText.toLowerCase())
    )
    .filter(item => {
      if (state.filters.status === 'completed') return item.completed
      if (state.filters.status === 'canceled') return item.canceled
      else return true
    })
    .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
}

// Сумма отфильтрованных выплат
export const paymentsSum = (state, getters) => getters.filteredPayments.reduce((prev, current) => prev + current.sum, 0)

// Количество отфильтрованных заказов
export const paymentsCount = (state, getters) => getters.filteredPayments.length
