export function mutationPayments (state, data) {
  state.payments = data
}

export function mutationsFilters (state, data) {
  state.canResetFilters = true
  state.filters[data.field] = data.value
}

export function mutationsCanResetFilters (state, data) {
  state.canResetFilters = data
}
