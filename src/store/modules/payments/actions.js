import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

export async function loadPayments ({ commit, state }, params = {}) {
  try {
    await axios
      .get(`${server.serverURI}/payments`, {
        headers: server.headers,
        params: {
          ...params,
          createdAt_gte: state.filters.dateFrom,
          createdAt_lte: state.filters.dateTo
        }
      })
      .then(response => {
        commit('mutationPayments', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
