export function mutationOrders (state, data) {
  state.orders = data
}

export function mutationOffer (state, data) {
  state.offer = data
}

export function mutationSeller (state, data) {
  state.seller = data
}

export function mutationsFilters (state, data) {
  state.canResetFilters = true
  state.filters[data.field] = data.value
}

export function mutationsCanResetFilters (state, data) {
  state.canResetFilters = data
}
