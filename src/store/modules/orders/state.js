export default function () {
  return {
    orders: [],
    canResetFilters: false,

    filters: {
      searchText: '',
      dateFrom: new Date(new Date().setHours(0)),
      dateTo: new Date(),
      status: 'all',
      statusOptions: [
        { label: 'Все заказы', value: 'all' },
        { label: 'Выполненные заказы', value: 'completed' },
        { label: 'Невыполненные заказы', value: 'notCompleted' },
        { label: 'Отмененные заказы', value: 'canceled' }
      ]
    }
  }
}
