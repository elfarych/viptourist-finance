export const filteredOrders = state => {
  return state.orders
    .filter(
      item => item.profile.name?.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.profile.id?.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.seller.id?.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.seller.name?.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.offer.tour.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.offer.id.toLowerCase().includes(state.filters.searchText.toLowerCase()) ||
        item.id.toLowerCase().includes(state.filters.searchText.toLowerCase())
    )
    .filter(item => {
      if (state.filters.status === 'completed') return item.activated
      if (state.filters.status === 'canceled') return item.canceled
      if (state.filters.status === 'notCompleted') return !item.activated && !item.canceled
      else return true
    })
    .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
}

// Сумма отфильтрованных заказов
export const ordersSum = (state, getters) => getters.filteredOrders.reduce((prev, current) => prev + current.price, 0)

// Сумма отфильтрованных заказов
export const ordersCommissionSum = (state, getters) => getters.filteredOrders.reduce((prev, current) => prev + current.commission, 0)

// Количество отфильтрованных заказов
export const ordersCount = (state, getters) => getters.filteredOrders.length
