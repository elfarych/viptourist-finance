import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import user from './modules/user'
import orders from './modules/orders'
import payments from './modules/payments'
import profile from './modules/profile'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      user,
      orders,
      payments,
      profile
    },

    strict: process.env.DEBUGGING
  })

  return Store
}
